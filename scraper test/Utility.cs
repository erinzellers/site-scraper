﻿using System;
using System.Collections.Generic;
using System.Text;

namespace scraper_test
{
    class Utility
    {
        public static string CleanDomainName(string domain)
        {
            //clean up uri
            System.Uri uri = new Uri(domain);
            string uriWithoutScheme = uri.Host.Replace("www.", "") + uri.PathAndQuery;

            //remove trailing slash if present
            uriWithoutScheme = uriWithoutScheme.EndsWith("/") ? uriWithoutScheme.Substring(0, uriWithoutScheme.Length - 1) : uriWithoutScheme;

            return uriWithoutScheme;
        }
    }
}
