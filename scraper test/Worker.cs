﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace scraper_test
{
    class Worker
    {
         public static void GenerateFileAndFolderStructure(string projectFolder, string fileBody, string url)
        {
            //based on the url being scraped create first child folder
            System.Uri uri = new Uri(url);

            string cleanUri = Utility.CleanDomainName(url);

            //second child folder
            string[] splitUri = cleanUri.Split("/");

            //get ultimate file name from uri and suffix
            string filename = splitUri[splitUri.Length - 1];

            //first level files



            //first level folders and files

            if (splitUri.Length < 2)
            {
                string path = projectFolder;

                string pathAndFileName = Path.Combine(projectFolder, "index");


                Generator(path, pathAndFileName, fileBody, splitUri, filename);

            }
            if (splitUri.Length == 2)
            {
                string path = System.IO.Path.Combine(projectFolder, splitUri[splitUri.Length - 2]);

                string pathAndFileName = Path.Combine(projectFolder, splitUri[splitUri.Length - 1]);

               
                Generator(path, pathAndFileName, fileBody, splitUri, filename);

            }
            if (splitUri.Length == 3)
            {
                string path = System.IO.Path.Combine(projectFolder, splitUri[splitUri.Length - 2]);

                string pathAndFileName = System.IO.Path.Combine(projectFolder, splitUri[splitUri.Length - 2], splitUri[splitUri.Length - 1]);

                Generator(path, pathAndFileName, fileBody, splitUri, filename);

            }
            if (splitUri.Length == 4)
            {
                string path = System.IO.Path.Combine(projectFolder, splitUri[splitUri.Length - 3], splitUri[splitUri.Length - 2]);
                
                string pathAndFileName = System.IO.Path.Combine(projectFolder, splitUri[splitUri.Length - 3], splitUri[splitUri.Length - 2], splitUri[splitUri.Length - 1]);               

                Generator(path, pathAndFileName, fileBody, splitUri, filename);
            }
            //else
            //{
            //    //top level items 

            //    string file = projectFolder + "/index.html";

            //    var html = File.Create(file);
            //    html.Close();

            //    TextWriter tw = new StreamWriter(file, true);
            //    tw.WriteLine(fileBody);
            //    tw.Close();
            //}

        }
        

        public static void WriteFile(string pathAndFileName, string fileBody)
        {
            System.IO.File.WriteAllText(pathAndFileName, fileBody);
        }

        private static void Generator(string path, string pathAndFileName, string fileBody, string[] splitUri, string filename)
        {
            //add second level folder if not exists
            if (!System.IO.Directory.Exists(path))
            {
                System.IO.Directory.CreateDirectory(path);
            }

            //if firstChildMatch has a file suffix (.asp) go ahead and create a new file with it
            Regex aspExtensionCheck = new Regex(@"(\.asp)$");
            if (aspExtensionCheck.IsMatch(pathAndFileName))
            {
                pathAndFileName = pathAndFileName.Remove(pathAndFileName.Length - 3, 3) + "html";
                WriteFile(pathAndFileName, fileBody);
                return;
            }            

            //add files to created dir 
            if (!System.IO.File.Exists(splitUri[splitUri.Length - 1]))
            {
                //if url contains extension skip this step
                if (!aspExtensionCheck.IsMatch(pathAndFileName))
                {
                    pathAndFileName = pathAndFileName + ".html";
                    WriteFile(pathAndFileName, fileBody);
                    return;
                }

                WriteFile(pathAndFileName, fileBody);
            }
            else
            {
                Console.WriteLine("File \"{0}\" already exists.", filename);
                return;
            }
        }
    }
}
