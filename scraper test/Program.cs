﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.NetworkInformation;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using HtmlAgilityPack;

namespace scraper_test
{
    class Program
    {
        static void Main(string[] args)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(@"C:\Users\ezellers\Desktop\sitemaps\airease-sitemap.xml");

            foreach (XmlNode node in doc.DocumentElement.ChildNodes)
            {
                //<url>
                foreach (XmlNode uri in node.ChildNodes)
                {
                    //<loc>

                    var url = uri.InnerText;

                    if (url.EndsWith(".pdf"))                    
                        continue;                    

                    HtmlWeb web = new HtmlWeb();

                    //load url from sitemap
                    var htmlDoc = web.Load(url);

                    //scrape html
                    var body = htmlDoc.DocumentNode.SelectSingleNode("/html"); // returns page div with contents :)

                    //create new file and copy scraped html into it
                    //Worker.GenerateFileAndFolderStructure(@"C:\wwwroot\allied-websites\AirEase\Views\Home", body.InnerHtml, url);
                    Worker.GenerateFileAndFolderStructure(@"C:\\Users\ezellers\Desktop\airease", body.InnerHtml, url);

                }
            }

            Console.WriteLine("#scraped");

            Console.ReadLine();
        }


    }
}
